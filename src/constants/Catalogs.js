import * as Constants from './Constants'

// 新型コロナワクチン接種回数
export const DefaultNumberOfInjection = Constants.NUMBER_OF_INJECTION_VALUE_ZERO
export const NumberOfInjection = [
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_ZERO,
    value: Constants.NUMBER_OF_INJECTION_VALUE_ZERO,
  },
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_ONE,
    value: Constants.NUMBER_OF_INJECTION_VALUE_ONE,
  },
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_TWO,
    value: Constants.NUMBER_OF_INJECTION_VALUE_TWO,
  },
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_THREE,
    value: Constants.NUMBER_OF_INJECTION_VALUE_THREE,
  },
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_FOUR,
    value: Constants.NUMBER_OF_INJECTION_VALUE_FOUR,
  },
  {
    label: Constants.NUMBER_OF_INJECTION_LABEL_MORE,
    value: Constants.NUMBER_OF_INJECTION_VALUE_MORE,
  },
]

// 新型コロナワクチン接種後経過日数
export const DefaultDaysAfterVaccinated = Constants.DAYS_AFTER_VACCINATED_VALUE_ZERO
export const DaysAfterVaccinated = [
  {
    label: Constants.DAYS_AFTER_VACCINATED_LABEL_ZERO,
    value: Constants.DAYS_AFTER_VACCINATED_VALUE_ZERO,
  },
  {
    label: Constants.DAYS_AFTER_VACCINATED_LABEL_ONE,
    value: Constants.DAYS_AFTER_VACCINATED_VALUE_ONE,
  },
  {
    label: Constants.DAYS_AFTER_VACCINATED_LABEL_WITHIN14,
    value: Constants.DAYS_AFTER_VACCINATED_VALUE_WITHIN14,
  },
  {
    label: Constants.DAYS_AFTER_VACCINATED_LABEL_AFTER14,
    value: Constants.DAYS_AFTER_VACCINATED_VALUE_AFTER14,
  },
]

// PCRの結果
export const DefaultPCRResult = Constants.PCR_RESULT_VALUE_NOT_TESTED
export const PCRResult = [
  {
    label: Constants.PCR_RESULT_LABEL_NOT_TESTED,
    value: Constants.PCR_RESULT_VALUE_NOT_TESTED,
  },
  {
    label: Constants.PCR_RESULT_LABEL_NEGATIVE,
    value: Constants.PCR_RESULT_VALUE_NEGATIVE,
  },
  {
    label: Constants.PCR_RESULT_LABEL_POSITIVE,
    value: Constants.PCR_RESULT_VALUE_POSITIVE,
  },
]

// 基礎疾患の有無
export const DefaultPreExistingCondition = Constants.PRE_EXISTING_CONDITION_VALUE_NO
export const PreExistingCondition = [
  {
    label: Constants.PRE_EXISTING_CONDITION_LABEL_NO,
    value: Constants.PRE_EXISTING_CONDITION_VALUE_NO,
  },
  {
    label: Constants.PRE_EXISTING_CONDITION_LABEL_YES,
    value: Constants.PRE_EXISTING_CONDITION_VALUE_YES,
  },
]

// 症状
export const DefaultSymptom = Constants.SYMPTOMS_VALUE_NONE
export const Symptom = [
  {
    label: Constants.SYMPTOMS_LABEL_NONE,
    value: Constants.SYMPTOMS_VALUE_NONE,
  },
  {
    label: Constants.SYMPTOMS_LABEL_COLD,
    value: Constants.SYMPTOMS_VALUE_COLD,
  },
  {
    label: Constants.SYMPTOMS_LABEL_ANAPHYLAXIS,
    value: Constants.SYMPTOMS_VALUE_ANAPHYLAXIS,
  },
  {
    label: Constants.SYMPTOMS_LABEL_MYOCARDITIS,
    value: Constants.SYMPTOMS_VALUE_MYOCARDITIS,
  },
  {
    label: Constants.SYMPTOMS_LABEL_CEREBRAL_INFARCTION,
    value: Constants.SYMPTOMS_VALUE_CEREBRAL_INFARCTION,
  },
  {
    label: Constants.SYMPTOMS_LABEL_DEATH,
    value: Constants.SYMPTOMS_VALUE_DEATH,
  },
]

// 死因
export const DefaultCauseOfDeath = Constants.CAUSE_OF_DEATH_VALUE_CHRONIC_CONDITION
export const CauseOfDeath = [
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_CHRONIC_CONDITION,
    value: Constants.CAUSE_OF_DEATH_VALUE_CHRONIC_CONDITION,
  },
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_CANCER,
    value: Constants.CAUSE_OF_DEATH_VALUE_CANCER,
  },
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_SENILITY,
    value: Constants.CAUSE_OF_DEATH_VALUE_SENILITY,
  },
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_TRAFFIC_ACCIDENT,
    value: Constants.CAUSE_OF_DEATH_VALUE_TRAFFIC_ACCIDENT,
  },
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_SUICIDE,
    value: Constants.CAUSE_OF_DEATH_VALUE_SUICIDE,
  },
  {
    label: Constants.CAUSE_OF_DEATH_LABEL_PNEUMONIA,
    value: Constants.CAUSE_OF_DEATH_VALUE_PNEUMONIA,
  },
]
