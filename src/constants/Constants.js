// 新型コロナワクチン接種回数
export const NUMBER_OF_INJECTION_LABEL_ZERO  = "未接種"
export const NUMBER_OF_INJECTION_LABEL_ONE   = "1回"
export const NUMBER_OF_INJECTION_LABEL_TWO   = "2回"
export const NUMBER_OF_INJECTION_LABEL_THREE = "3回"
export const NUMBER_OF_INJECTION_LABEL_FOUR  = "4回"
export const NUMBER_OF_INJECTION_LABEL_MORE  = "それ以上"

export const NUMBER_OF_INJECTION_VALUE_ZERO  = "zero"
export const NUMBER_OF_INJECTION_VALUE_ONE   = "one"
export const NUMBER_OF_INJECTION_VALUE_TWO   = "two"
export const NUMBER_OF_INJECTION_VALUE_THREE = "three"
export const NUMBER_OF_INJECTION_VALUE_FOUR  = "four"
export const NUMBER_OF_INJECTION_VALUE_MORE  = "more"

// 新型コロナワクチン接種後経過日数
export const DAYS_AFTER_VACCINATED_LABEL_ZERO     = "接種当日"
export const DAYS_AFTER_VACCINATED_LABEL_ONE      = "1日"
export const DAYS_AFTER_VACCINATED_LABEL_WITHIN14 = "14日以内"
export const DAYS_AFTER_VACCINATED_LABEL_AFTER14 = "14日以降"

export const DAYS_AFTER_VACCINATED_VALUE_ZERO     = "zero"
export const DAYS_AFTER_VACCINATED_VALUE_ONE      = "one"
export const DAYS_AFTER_VACCINATED_VALUE_WITHIN14 = "within14"
export const DAYS_AFTER_VACCINATED_VALUE_AFTER14  = "after14"

// PCRの結果
export const PCR_RESULT_LABEL_NOT_TESTED   = "未実施"
export const PCR_RESULT_LABEL_NEGATIVE = "陰性"
export const PCR_RESULT_LABEL_POSITIVE = "陽性"

export const PCR_RESULT_VALUE_NOT_TESTED   = "notTested"
export const PCR_RESULT_VALUE_NEGATIVE = "negative"
export const PCR_RESULT_VALUE_POSITIVE = "positive"

// 基礎疾患の有無
export const PRE_EXISTING_CONDITION_LABEL_NO  = "基礎疾患なし"
export const PRE_EXISTING_CONDITION_LABEL_YES = "基礎疾患あり"

export const PRE_EXISTING_CONDITION_VALUE_NO  = "no"
export const PRE_EXISTING_CONDITION_VALUE_YES = "yes"

// 症状
export const SYMPTOMS_LABEL_NONE                = "無症状"
export const SYMPTOMS_LABEL_COLD                = "39℃の風邪"
export const SYMPTOMS_LABEL_ANAPHYLAXIS         = "アナフィラキシー"
export const SYMPTOMS_LABEL_MYOCARDITIS         = "心筋炎"
export const SYMPTOMS_LABEL_CEREBRAL_INFARCTION = "脳梗塞"
export const SYMPTOMS_LABEL_DEATH               = "死亡"

export const SYMPTOMS_VALUE_NONE                = "none"
export const SYMPTOMS_VALUE_COLD                = "cold"
export const SYMPTOMS_VALUE_ANAPHYLAXIS         = "anaphylaxis"
export const SYMPTOMS_VALUE_MYOCARDITIS         = "myocarditis"
export const SYMPTOMS_VALUE_CEREBRAL_INFARCTION = "cerebralInfarction"
export const SYMPTOMS_VALUE_DEATH               = "death"

// 死因
export const CAUSE_OF_DEATH_LABEL_CHRONIC_CONDITION = "持病"
export const CAUSE_OF_DEATH_LABEL_CANCER            = "癌"
export const CAUSE_OF_DEATH_LABEL_SENILITY          = "老衰"
export const CAUSE_OF_DEATH_LABEL_TRAFFIC_ACCIDENT  = "交通事故"
export const CAUSE_OF_DEATH_LABEL_SUICIDE           = "自殺"
export const CAUSE_OF_DEATH_LABEL_PNEUMONIA         = "コロナ感染症による肺炎"

export const CAUSE_OF_DEATH_VALUE_CHRONIC_CONDITION = "chronicCondition"
export const CAUSE_OF_DEATH_VALUE_CANCER            = "cancer"
export const CAUSE_OF_DEATH_VALUE_SENILITY          = "senility"
export const CAUSE_OF_DEATH_VALUE_TRAFFIC_ACCIDENT  = "trafficAccident"
export const CAUSE_OF_DEATH_VALUE_SUICIDE           = "suicide"
export const CAUSE_OF_DEATH_VALUE_PNEUMONIA         = "pneumonia"
