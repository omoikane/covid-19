// react
import React, { Component } from "react"
import { BrowserRouter, Route, Routes, Redirect } from "react-router-dom"
import App from './pages/App'

class AppRoutes extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="main-content">
          <Routes>
            <Route path="/covid-19" exact element={<App />} />
          </Routes>
        </div>
      </BrowserRouter>
    )
  }
}

export default AppRoutes
