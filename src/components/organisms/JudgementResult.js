import * as React from 'react'
import PropTypes from 'prop-types'

import Typography from '@mui/material/Typography'

import {
  NUMBER_OF_INJECTION_VALUE_ZERO,
  DAYS_AFTER_VACCINATED_VALUE_AFTER14,

  PCR_RESULT_VALUE_NOT_TESTED,
  PCR_RESULT_VALUE_NEGATIVE,
  PCR_RESULT_VALUE_POSITIVE,

  PRE_EXISTING_CONDITION_VALUE_NO,
  SYMPTOMS_VALUE_NONE,
  SYMPTOMS_LABEL_DEATH,
  SYMPTOMS_VALUE_DEATH,
  CAUSE_OF_DEATH_LABEL_PNEUMONIA,
  CAUSE_OF_DEATH_VALUE_PNEUMONIA,
} from '../../constants/Constants'
import {
  CauseOfDeath,
  NumberOfInjection,
  Symptom,
} from '../../constants/Catalogs'
import Title from '../atoms/Title'
import Alert from '../atoms/Alert'

function Judgement(
  numberOfInjection,
  daysAfterVaccinated,
  pcrResult,
  preExistingCondition,
  symptom,
  causeOfDeath,
) {
  const results = {
    findings: {
      severity: "success",
      value: "健康",
    },
    causeOfDeath: {
      severity: "success",
      value: "",
    },
    injectionCondition: {
      severity: "success",
      value: "不明",
    },
    consideration: {
      severity: "success",
      value: "",
    }
  }

  // 医師の所見の特定
  if (symptom === SYMPTOMS_VALUE_DEATH) {
    results.findings.value = SYMPTOMS_LABEL_DEATH
    results.findings.severity = 'error'
  } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
    results.findings.value = 'コロナ感染症'
    results.findings.severity = 'error'
  } else if (numberOfInjection !== NUMBER_OF_INJECTION_VALUE_ZERO){
    const targetSymptom = Symptom.find(data => data.value === symptom)
    if (targetSymptom) {
      results.findings.value = targetSymptom.label
    }
  } else if (symptom !== SYMPTOMS_VALUE_NONE) {
    switch (pcrResult) {
      case PCR_RESULT_VALUE_NOT_TESTED:
        results.findings.value = 'コロナの疑いありPCR検査を受けてください'
        results.findings.severity = 'warning'
        break;
      case PCR_RESULT_VALUE_NEGATIVE:
        results.findings.value = 'コロナ感染症(みなし陽性)'
        results.findings.severity = 'error'
        break;
    }
  }
  
  // 死因の特定
  if (symptom === SYMPTOMS_VALUE_DEATH) {
    if (causeOfDeath === CAUSE_OF_DEATH_VALUE_PNEUMONIA) {
      results.causeOfDeath.severity = 'error'
      results.causeOfDeath.value = CAUSE_OF_DEATH_LABEL_PNEUMONIA
      results.causeOfDeath.value = CauseOfDeath.find(data => data.value === causeOfDeath).label
    } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
      results.causeOfDeath.severity = 'error'
      results.causeOfDeath.value = "コロナ感染症による死亡"
    } else {
      const targetCause = CauseOfDeath.find(data => data.value === causeOfDeath)
      if (targetCause) {
        results.causeOfDeath.value = targetCause.label
      }
    }
  }

  // ワクチン接種報告
  if (numberOfInjection === NUMBER_OF_INJECTION_VALUE_ZERO){
    if (
      symptom !== SYMPTOMS_VALUE_NONE
      || pcrResult === PCR_RESULT_VALUE_POSITIVE
    ) {
      results.injectionCondition.severity = 'error'
      results.injectionCondition.value = "ワクチン未接種"
    }
  } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
    switch (daysAfterVaccinated) {
      case DAYS_AFTER_VACCINATED_VALUE_AFTER14:
        results.injectionCondition.severity = 'warning'
        results.injectionCondition.value = "非公開"
        break;
      default:
        results.injectionCondition.severity = 'error'
        results.injectionCondition.value = "ワクチン未接種"
        break;
    }
  } else {
    const targetNumber = NumberOfInjection.find(data => data.value === numberOfInjection)
    if (targetNumber) {
      results.injectionCondition.value = targetNumber.label
    }
  }

  if (preExistingCondition !== PRE_EXISTING_CONDITION_VALUE_NO) {
  }
  
  // 考察
  // consideration, ワクチン未接種のため悪化。ワクチンのおかげで重症化を防いだ


  return results
}

function Consideration(
  numberOfInjection,
  daysAfterVaccinated,
  pcrResult,
  preExistingCondition,
  symptom,
  causeOfDeath,
) {
  let result = "なし"
  result = "なし"

  return result

  const results = {
    findings: {
      severity: "success",
      value: "健康",
    },
    causeOfDeath: {
      severity: "success",
      value: "",
    },
    injectionCondition: {
      severity: "success",
      value: "不明",
    },
    consideration: {
      severity: "success",
      value: "",
    }
  }

  // 医師の所見の特定
  if (symptom === SYMPTOMS_VALUE_DEATH) {
    results.findings.value = SYMPTOMS_LABEL_DEATH
    results.findings.severity = 'error'
  } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
    results.findings.value = 'コロナ感染症'
    results.findings.severity = 'error'
  } else if (numberOfInjection !== NUMBER_OF_INJECTION_VALUE_ZERO){
    const targetSymptom = Symptom.find(data => data.value === symptom)
    if (targetSymptom) {
      results.findings.value = targetSymptom.label
    }
  } else if (symptom !== SYMPTOMS_VALUE_NONE) {
    switch (pcrResult) {
      case PCR_RESULT_VALUE_NOT_TESTED:
        results.findings.value = 'コロナの疑いありPCR検査を受けてください'
        results.findings.severity = 'warning'
        break;
      case PCR_RESULT_VALUE_NEGATIVE:
        results.findings.value = 'コロナ感染症(みなし陽性)'
        results.findings.severity = 'error'
        break;
    }
  }
  
  // 死因の特定
  if (symptom === SYMPTOMS_VALUE_DEATH) {
    if (causeOfDeath === CAUSE_OF_DEATH_VALUE_PNEUMONIA) {
      results.causeOfDeath.severity = 'error'
      results.causeOfDeath.value = CAUSE_OF_DEATH_LABEL_PNEUMONIA
      results.causeOfDeath.value = CauseOfDeath.find(data => data.value === causeOfDeath).label
    } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
      results.causeOfDeath.severity = 'error'
      results.causeOfDeath.value = "コロナ感染症による死亡"
    } else {
      const targetCause = CauseOfDeath.find(data => data.value === causeOfDeath)
      if (targetCause) {
        results.causeOfDeath.value = targetCause.label
      }
    }
  }

  // ワクチン接種報告
  if (numberOfInjection === NUMBER_OF_INJECTION_VALUE_ZERO){
    if (
      symptom !== SYMPTOMS_VALUE_NONE
      || pcrResult === PCR_RESULT_VALUE_POSITIVE
    ) {
      results.injectionCondition.severity = 'error'
      results.injectionCondition.value = "ワクチン未接種"
    }
  } else if (pcrResult === PCR_RESULT_VALUE_POSITIVE) {
    switch (daysAfterVaccinated) {
      case DAYS_AFTER_VACCINATED_VALUE_AFTER14:
        results.injectionCondition.severity = 'warning'
        results.injectionCondition.value = "非公開"
        break;
      default:
        results.injectionCondition.severity = 'error'
        results.injectionCondition.value = "ワクチン未接種"
        break;
    }
  } else {
    const targetNumber = NumberOfInjection.find(data => data.value === numberOfInjection)
    if (targetNumber) {
      results.injectionCondition.value = targetNumber.label
    }
  }

  if (preExistingCondition !== PRE_EXISTING_CONDITION_VALUE_NO) {
  }
  
  // 考察
  // consideration, ワクチン未接種のため悪化。ワクチンのおかげで重症化を防いだ


  return results
}

function JudgementResult(props) {
  const results = Judgement(
    props.numberOfInjection,
    props.daysAfterVaccinated,
    props.pcrResult,
    props.preExistingCondition,
    props.symptom,
    props.causeOfDeath,
  )
  
  return (
    <React.Fragment>
      <Title>結果</Title>

      <Typography component="p" variant="h4">
        医師の所見
      </Typography>
      <Alert severity={results.findings.severity}>{results.findings.value}</Alert>

      { props.symptom === SYMPTOMS_VALUE_DEATH &&
        <React.Fragment>
          <Typography component="p" variant="h4">
            死因
          </Typography>
          <Alert severity={results.causeOfDeath.severity}>{results.causeOfDeath.value}</Alert>
        </React.Fragment>
      }

      <Typography component="p" variant="h4">
        ワクチン接種報告
      </Typography>
      <Alert severity={results.injectionCondition.severity}>{results.injectionCondition.value}</Alert>

    </React.Fragment>
  )
}

JudgementResult.propTypes = {
  numberOfInjection: PropTypes.string,
  daysAfterVaccinated: PropTypes.string,
  pcrResult: PropTypes.string,
  preExistingCondition: PropTypes.string,
  symptom: PropTypes.string,
  causeOfDeath: PropTypes.string,
}
export default JudgementResult
