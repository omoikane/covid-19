import * as React from 'react'
import PropTypes from 'prop-types'

import {
  NUMBER_OF_INJECTION_VALUE_ZERO,
  SYMPTOMS_VALUE_DEATH,
} from '../../constants/Constants'

import Title from '../atoms/Title'
import RadioNumberOfInjection from '../molecules/RadioNumberOfInjection'
import RadioDaysAfterVaccinated from '../molecules/RadioDaysAfterVaccinated'
import RadioPCRResult from '../molecules/RadioPCRResult'
import RadioPreExistingConditions from '../molecules/RadioPreExistingConditions'
import RadioSymptoms from '../molecules/RadioSymptoms'
import RadioCauseOfDeath from '../molecules/RadioCauseOfDeath'

import Box from '@mui/material/Box'

function JudgementConditions(props) {
  const [numberOfInjection, setNumberOfInjection] = React.useState(null)
  const [symptom, setSymptom] = React.useState(null)

  const numberOfInjectionHandleChange = value => {
    props.numberOfInjectionHandleChange(value)
    setNumberOfInjection(value)
  }
  const symptomHandleChange = value => {
    props.symptomHandleChange(value)
    setSymptom(value)
  }

  return (
    <React.Fragment>
      <Title>判定条件</Title>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete="off"
      >
        <div>
          <RadioSymptoms
            handleChange={symptomHandleChange}
          />
          {symptom === SYMPTOMS_VALUE_DEATH &&
           <RadioCauseOfDeath
             handleChange={props.causeOfDeathHandleChange}
           />
          }
          <RadioPCRResult
            handleChange={props.pcrResultHandleChange}
          />
          <RadioNumberOfInjection
            handleChange={numberOfInjectionHandleChange}
          />
          {numberOfInjection !== NUMBER_OF_INJECTION_VALUE_ZERO &&
           <RadioDaysAfterVaccinated
             handleChange={props.daysAfterVaccinatedHandleChange}
           />
          }
          <RadioPreExistingConditions
            handleChange={props.preExistingConditionHandleChange}
          />
        </div>
      </Box>
    </React.Fragment>
  )
}

JudgementConditions.propTypes = {
  numberOfInjectionHandleChange: PropTypes.func.isRequired,
  daysAfterVaccinatedHandleChange: PropTypes.func.isRequired,
  pcrResultHandleChange: PropTypes.func.isRequired,
  preExistingConditionHandleChange: PropTypes.func.isRequired,
  symptomHandleChange: PropTypes.func.isRequired,
  causeOfDeathHandleChange: PropTypes.func.isRequired,
}
export default JudgementConditions
