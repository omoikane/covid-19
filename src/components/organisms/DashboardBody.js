import * as React from 'react'

import { styled, createTheme, ThemeProvider } from '@mui/material/styles'
import Container from '@mui/material/Container'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'

import Copyright from '../atoms/Copyright'
import JudgementConditions from '../organisms/JudgementConditions'
import JudgementResult from '../organisms/JudgementResult'

function DashboardContent() {
  const [numberOfInjection, setNumberOfInjection] = React.useState(null)
  const [daysAfterVaccinated, setDaysAfterVaccinated] = React.useState(null)
  const [pcrResult, setPcrResult] = React.useState(null)
  const [preExistingCondition, setPreExistingCondition] = React.useState(null)
  const [symptom, setSymptom] = React.useState(null)
  const [causeOfDeath, setCauseOfDeath] = React.useState(null)

  const numberOfInjectionHandleChange = value => {
    setNumberOfInjection(value)
  }
  const daysAfterVaccinatedHandleChange = value => {
    setDaysAfterVaccinated(value)
  }
  const pcrResultHandleChange = value => {
    setPcrResult(value)
  }
  const preExistingConditionHandleChange = value => {
    setPreExistingCondition(value)
  }
  const symptomHandleChange = value => {
    setSymptom(value)
  }
  const causeOfDeathHandleChange = value => {
    setCauseOfDeath(value)
  }

  return (
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
      <Grid container spacing={3}>
        {/* JudgementResult */}
        <Grid item xs={12}>
          <Paper
            sx={{
              p: 2,
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <JudgementResult
              numberOfInjection={numberOfInjection}
              daysAfterVaccinated={daysAfterVaccinated}
              pcrResult={pcrResult}
              preExistingCondition={preExistingCondition}
              symptom={symptom}
              causeOfDeath={causeOfDeath}
            />
          </Paper>
        </Grid>
        {/* JudgementConditions */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <JudgementConditions
              numberOfInjectionHandleChange={numberOfInjectionHandleChange}
              daysAfterVaccinatedHandleChange={daysAfterVaccinatedHandleChange}
              pcrResultHandleChange={pcrResultHandleChange}
              preExistingConditionHandleChange={preExistingConditionHandleChange}
              symptomHandleChange={symptomHandleChange}
              causeOfDeathHandleChange={causeOfDeathHandleChange}
            />
          </Paper>
        </Grid>
      </Grid>
      <Copyright sx={{ pt: 4 }} />
    </Container>
  )
}

export default function Dashboard() {
  return <DashboardContent />
}
