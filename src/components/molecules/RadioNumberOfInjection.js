import * as React from 'react'
import PropTypes from 'prop-types'

import {
  NumberOfInjection as objects,
  DefaultNumberOfInjection as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioNumberOfInjection(props) {
  const formLabel = "新型コロナワクチンの接種回数"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}
RadioNumberOfInjection.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioNumberOfInjection
