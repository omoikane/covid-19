import * as React from 'react'
import PropTypes from 'prop-types'

import {
  DaysAfterVaccinated as objects,
  DefaultDaysAfterVaccinated as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioDaysAfterVaccinated(props) {
  const formLabel = "新型コロナワクチン接種後経過日数"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}

RadioDaysAfterVaccinated.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioDaysAfterVaccinated
