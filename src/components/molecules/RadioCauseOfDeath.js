import * as React from 'react'
import PropTypes from 'prop-types'

import {
  CauseOfDeath as objects,
  DefaultCauseOfDeath as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioCauseOfDeath(props) {
  const formLabel = "死因"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}

RadioCauseOfDeath.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioCauseOfDeath
