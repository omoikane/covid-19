import * as React from 'react'
import PropTypes from 'prop-types'

import {
  PCRResult as objects,
  DefaultPCRResult as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioPCRResult(props) {
  const formLabel = "PCRの結果"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}

RadioPCRResult.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioPCRResult
