import * as React from 'react'
import PropTypes from 'prop-types'

import Radio from '@mui/material/Radio'
import MyRadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'

function RadioGroup(props) {
  const [val, setVal] = React.useState(props.defaultValue)

  React.useEffect(() => {
    // Your code here
    props.handleChange(val)
  }, [])

  const handleChange = e => {
    props.handleChange(e.target.value)
    setVal(e.target.value)
  }

  return (
    <React.Fragment>
      <FormLabel id="demo-radio-buttons-group-label">{props.formLabel}</FormLabel>
      <MyRadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
        onChange={handleChange}
      >
        {props.objects.map((data) => {
           return <FormControlLabel
                    key={data.label}
                    control={<Radio />}
                    label={data.label}
                    value={data.value}
                    checked={data.value === val}
           />
        })}
      </MyRadioGroup>
    </React.Fragment>
  )
}

RadioGroup.propTypes = {
  formLabel: PropTypes.string.isRequired,
  objects: PropTypes.array.isRequired,
  defaultValue: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
}
export default RadioGroup
