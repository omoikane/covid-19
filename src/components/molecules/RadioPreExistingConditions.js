import * as React from 'react'
import PropTypes from 'prop-types'

import {
  PreExistingCondition as objects,
  DefaultPreExistingCondition as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioPreExistingConditions(props) {
  const formLabel = "基礎疾患の有無"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}

RadioPreExistingConditions.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioPreExistingConditions
