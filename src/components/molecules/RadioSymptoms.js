import * as React from 'react'
import PropTypes from 'prop-types'

import {
  Symptom as objects,
  DefaultSymptom as defaultValue
} from '../../constants/Catalogs'
import RadioGroup from './RadioGroup'

function RadioSymptoms(props) {
  const formLabel = "症状"
  return (
    <RadioGroup
      formLabel={formLabel}
      objects={objects}
      defaultValue={defaultValue}
      handleChange={props.handleChange}
    />
  )
}

RadioSymptoms.propTypes = {
  handleChange: PropTypes.func.isRequired,
}
export default RadioSymptoms
