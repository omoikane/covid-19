import * as React from 'react'
import PropTypes from 'prop-types'

import Stack from '@mui/material/Stack'
import MuiAlert from '@mui/material/Alert'

function Alert({
  children,
  severity,
}) {
  return (
    <Stack spacing={2} >
      <MuiAlert severity={severity}>{children}</MuiAlert>
    </Stack>
  )
}

Alert.propTypes = {
  severity: PropTypes.oneOf(['error', 'warning', 'info', 'success']).isRequired,
}
export default Alert
