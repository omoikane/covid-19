import * as React from 'react'

import MyTextField from '@mui/material/TextField'

export default function TextField(props) {
  return (
    <MyTextField
      id="outlined-required"
      label={props.label}
    />
  )
}
